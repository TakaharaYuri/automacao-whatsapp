from subprocess import Popen
import sys
from base64 import decodestring
import pyperclip
import base64
import time 
import requests
import os
import datetime
import logging
import gc
import psutil
import urllib


def log(acao, tipo = 'LOG'):
	try:
		now = datetime.datetime.now()
		now = now.strftime("%d/%m/%Y %H:%M:%S")
		now = str(now)
		gc.collect()
		process = psutil.Process(os.getpid())
		process = process.memory_info().rss/1024

		print(tipo+'['+now+']'+ '-> '+ acao)
	except Exception as ex:
		log(str(ex))

def init(trys):
	key = sys.argv[1]
	path = "http://crm2.marketingmidia9.com.br/whatsapp/sync/" + key
	try:
		retorno = requests.get(path).json()
		
		if (retorno['result'] == True):
			trys = trys + 1
			log('NOVA ENTRADA NA API ['  + str(trys) + ']')

			if retorno['msg']:
				retorno['msg'] = urllib.parse.quote(retorno['msg'])
			
			if (retorno['acao'] == 'send_image' or retorno['acao'] == 'change_profile_image'):
				file  = os.getcwd()+"\\images\\1_image.png"
				urllib.request.urlretrieve(retorno['img'], file)
				# with open(file,"wb") as f:
					# f.write(base64.b64decode(retorno['img']))

				command = 'bot.py --numero='+str(retorno['number'])+' --acao="'+str(retorno['acao'])+'" --msg="'+str(retorno['msg'])+'" --data="'+str(file)+'"'

			else:
				command = 'bot.py --numero='+str(retorno['number'])+' --acao="'+str(retorno['acao'])+'" --msg="'+str(retorno['msg'])+'"'

				
			p = Popen("python " + command, shell=True)
			p.wait()
			init(trys)

		else:
			log('NENHUMA ENTRADA NA API | MSG ENVIADAS: [' + str(trys) + ']')
			time.sleep(5)
			init(trys)

	except Exception as ex:
		print('PROBLEMA AO CONECTAR A API ' + str(ex))
		pass

log(' ===== INICIANDO APLICACAO ======')
init(0)

