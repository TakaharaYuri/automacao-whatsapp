import psutil
import datetime 
import gc
import os
class Log():
    def __init__(self):
        self = self
        self.instance = False
    
    def show(self, acao, tipo = 'LOG'):
        now = datetime.datetime.now()
        now = now.strftime("%H:%M:%S")
        now = str(now)
        gc.collect()
        process = psutil.Process(os.getpid())
        process = process.memory_info().rss/1024
        print(tipo+'['+now+']'+ '['+str(self.instance)+']' +'-> '+ str(acao) + ' | ' + str(process) + 'KB')
    
    def now_minutes(self):
        now = datetime.datetime.now()
        now = now.strftime("%H:%M")
        now = str(now)
        return now
