from selenium import webdriver 
from selenium.webdriver.chrome.options import Options
import urllib.parse
import argparse
import time 
import sys
import log
import os
import requests
import base64
import random
from whatsapp import whatsapp
from utilities import evo


def init(acao, numero="", msg="", data="", _id="", key = "", timer=4, nome_contato = ""):
    try:
        whatsapp.url = "https://web.whatsapp.com/"
        whatsapp._id = _id
        whatsapp.api_path = "http://crm2.marketingmidia9.com.br/whatsapp/"
        whatsapp.api_key = key
        evo.api_key = key

        # Listning chat, after execute services
        listening = whatsapp.listening_chats()

        if (listening):
            if (whatsapp.me_send_last_msg()):
                whatsapp.open_chat_info()
                receive_numero = whatsapp.get_chat_number()
                receive_msg = whatsapp.get_last_message()
                receive_type_msg = whatsapp.get_conversation()


                evo.api_whatsapp_reply(receive_numero, receive_msg)

                try:
                    whatsapp.send_text_message(receive_type_msg)
                except Exception as ex:
                    log.show('ERRO GENERICO 1 ->' + str(ex))
                    pass

        else:
            # Verifica se o Chat está ativo em alguma conversa
            # Para ficar monitorando os pontos internos e externos das conversas
            if (whatsapp.is_active_chat()):
                if (whatsapp.me_send_last_msg()):
                    msg = whatsapp.get_last_message()

                    type_msg = whatsapp.get_conversation()
                    log.show(type_msg)

                    try:
                        whatsapp.send_text_message(type_msg)
                    except Exception as ex:
                        log.show('ERRO GENERICO 1 ->' + str(ex))
                        pass

        if acao == 'send_message':
            whatsapp.insert_contact(str(numero))
            log.show('Aguardando timer -> ' + str(timer))
            time.sleep(timer)
            if whatsapp.validate_contact(_id):
                whatsapp.send_text_message(msg)
            else:
                return False

        if acao == 'send_contact':
            whatsapp.insert_contact(str(numero))
            log.show('Aguardando timer -> ' + str(timer))
            time.sleep(timer)
            if whatsapp.validate_contact(_id):
                handshake = whatsapp.get_handshake()
                whatsapp.send_text_message(handshake)
                whatsapp.send_text_message(msg)
                whatsapp.send_contact(nome_contato)
            else:
                return False

        if acao == 'send_image':
            whatsapp.insert_contact(str(numero))
            log.show('Aguardando timer -> ' + str(timer))
            time.sleep(timer)

            if whatsapp.validate_contact(_id):
                handshake = whatsapp.get_handshake()
                whatsapp.send_text_message(handshake)
                whatsapp.send_image_message(data, msg)
                thank_msg = whatsapp.get_thank_msg()
                whatsapp.send_text_message(thank_msg)
            else:
                return False
        if acao == 'change_profile_image':
            whatsapp.change_profile_image(data)
        
        if acao == 'change_profile_name':
            whatsapp.change_profile_name(msg)

        if acao == 'change_profile_status':
            whatsapp.change_profile_status(msg)

        if acao == 'check_last_msg':
            check = whatsapp.me_send_last_msg()
            
            if check:
                msg = whatsapp.get_last_message()
                log.show('Resultado -> ' + str(check))
                log.show('Resultado -> ' + str(msg))
            else:
                log.show('A ultima mensagem foi enviada por voce, aguarde nova entrada')
        
        if acao == 'listening_chats':
            while True:
                time.sleep(2)
                listening = whatsapp.listening_chats()

                if (listening):
                    time.sleep(1)
                    if (whatsapp.me_send_last_msg()):
                        whatsapp.open_chat_info()
                        numero = whatsapp.get_chat_number()
                        msg = whatsapp.get_last_message()
                        type_msg = whatsapp.get_conversation()

                        try:
                            # whatsapp.send_text_message(type_msg)
                            whatsapp.send_contact(nome_contato)
                        except Exception as ex:
                            log.show('ERRO GENERICO 1 ->' + str(ex))

                else:
                    # Verifica se o Chat está ativo em alguma conversa
                    # Para ficar monitorando os pontos internos e externos das conversas
                    if (whatsapp.is_active_chat()):
                        msg = whatsapp.get_last_message()

                        type_msg = whatsapp.get_conversation()
                        log.show(type_msg)

                        try:
                            # whatsapp.send_text_message(type_msg)
                            whatsapp.send_contact(nome_contato)
                        except Exception as ex:
                            log.show('ERRO GENERICO 1 ->' + str(ex))
                            


                
                
                time.sleep(2)
        
        if acao == 'debug':
            log.show(whatsapp.get_conversation())
        



        
        
        time.sleep(2)
    except Exception as ex:
        log.show('ERRO GENERICO ->' + str(ex))

def run(trys, key):
    path = "http://crm2.marketingmidia9.com.br/whatsapp/sync_v2/" + key
    try:
        retorno = requests.get(path).json()
        
        if (retorno['result'] == True):
            trys = trys + 1

            _timer = random.randint(10,15)
            log.show('NOVA ENTRADA NA API ['  + str(trys) + ']')
            
            if retorno['msg']:
                retorno['msg'] = urllib.parse.quote(retorno['msg'])
            
            if (retorno['acao'] == 'send_image' or retorno['acao'] == 'change_profile_image'):
                file  = os.getcwd()+"\\images\\image.png"
                urllib.request.urlretrieve(retorno['img'], file)
                
                init(retorno['acao'], retorno['number'], retorno['msg'], str(file), retorno['id'], key, _timer, retorno['nome_contato'])
                run(trys, key)
                
            else:
                init(retorno['acao'], retorno['number'], retorno['msg'], None, retorno['id'], key, _timer, retorno['nome_contato'])
                run(trys, key)
                
        
        else:
            log.show('NENHUMA ENTRADA NA API | MSG ENVIADAS: [' + str(trys) + ']')
            time.sleep(2)
            run(trys, key)
        
    
    except Exception as ex:
        print('PROBLEMA AO CONECTAR A API ' + str(ex))
        pass


parser = argparse.ArgumentParser()
parser.add_argument('--acao', required=False)
parser.add_argument('--numero', required=False)
parser.add_argument('--msg', required=False)
parser.add_argument('--data', required=False)
parser.add_argument('--headless', required=False, default=True)
parser.add_argument('--key', required=False)
parser.add_argument('--instance', required=False, default=0)

args = parser.parse_args()

# Chrome Config

options = Options() 
options.add_argument("user-data-dir="+os.getcwd()+"\\"+ args.instance +'_'+ args.key)
options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
options.add_argument("disable-infobars")
options.add_argument("log-level=3")
options.add_argument("disable-popup-blocking")
options.add_argument('disable-web-security')
options.add_argument('--disable-application-cache')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

if args.headless == True:
    options.add_argument("--headless")


driver = webdriver.Chrome(options=options, executable_path=os.getcwd()+"\\drivers\\chromedriver.exe")
driver.execute_script("window.onbeforeunload = function() {};")

log = log.Log()
log.instance = args.instance

whatsapp = whatsapp(driver)
evo = evo(driver)



login = whatsapp.login()
if (login == True):
    run(0, args.key)

driver.quit()

