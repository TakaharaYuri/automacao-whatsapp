from selenium.webdriver.common.keys import Keys 
from utilities import evo
from log import Log
import urllib
import time 
import os
import random
import requests


class whatsapp(object):

    def __init__(self, driver):
        self.driver = driver
        self.log = Log()
        self.evo = evo(driver)
        self.url = "https://web.whatsapp.com"
        self.api_path = False
        self._id = False
        self.api_key = False
        self.evo.api_key = self.api_key

    def login(self):
        self.log.show('Fazendo Login...')
        self.driver.get(self.url)
        self.driver.execute_script("window.onbeforeunload = function() {};")
        self.log.show('Iniciando tela de Login')
        self.log.show('Aguardando Leitura do QRCode')

        try:
            self.evo.wait_for_selector('#side > div._3CPl4 > div > label > input', 60)
            self.log.show('Whatsapp Autenticado com Sucesso')
            self.log.show('Dados gravados em whatsapp.profile')
            return True

        except:
            self.log.show('Nao foi possivel autenticar sua conta...')

            try:
                self.evo.wait_for_selector('#app > div > div > div > div > div > div > div._2Qffr', 5)
                txt_retorno = self.evo.get_text_by_selector('#app > div > div > div > div > div > div > div._2Qffr')
                self.log.show('Erro -> ' + txt_retorno)
                return False

            except:
                self.log.show('Nao foi possivel autenticar sua conta, Erro Generico')
                self.login()
                return False

    def check_login(self):
        self.log.show('Iniciando tela do Whatsapp')
        self.log.show('Aguardando Confirmacao de Login')
        try:
            self.evo.wait_for_selector('#side > header > div._2umId > div', 30)
            return True

        except:
            self.log.show('Nao foi Possivel confirmar o Login')
            self.check_login()

    def navigate(self, number, text=""):
        self.driver.get('https://web.whatsapp.com/send?phone='+str(number) + '&text=' + str(text))
        self.driver.execute_script("window.onbeforeunload = function() {};")

    def change_profile_name(self, name, callback=False):
        self.log.show('ACAO -> Alterar nome do Perfil')
        name = urllib.parse.unquote(name)
        if self.check_login():
            self.log.show('Iniciando tela do Whatsapp')
            self.log.show('Aguardando Confirmacao de Perfil')

            try:
                self.evo.wait_for_selector('#side > header > div._2umId > div', 30)
                self.driver.find_element_by_css_selector('#side > header > div._2umId > div').click()
                self.log.show('Clicando no Perfil')
                time.sleep(1)

                try:
                    self.evo.wait_for_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L > div._1DTd4._1G2k- > div > div._2S1VP.copyable-text.selectable-text', 10)
                    self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L > div._1DTd4._1G2k- > span.mOUqK > div > span').click()
                    
                    try:
                        self.evo.wait_for_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text', 10)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(Keys.CONTROL + 'a')
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(Keys.BACKSPACE)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(name)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(2) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(Keys.ENTER)
                        time.sleep(1)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > header > div > div.SFEHG > button > span').click()
                        self.log.show('Nome Alterar para ' + name + ' Com Sucesso')
                        self.evo.api_update_status(self._id, '6')
                        
                        try:
                            if callback != False:
                                callback(self) 
                        except Exception as ex:
                            self.log.show('ERRO Callback ->' + ex)            

                    except Exception as ex:
                        self.log.show(ex)
                        self.evo.api_update_status(self._id, '-1')
                    
                except Exception as ex:
                    self.log.show('Erro ao Carregar DIV do Perfil')
                    self.log.show(ex)
                    self.evo.api_update_status(self._id, '-1')
            except:
                self.log.show('Nao foi Possivel clicar no Perfil')
                self.evo.api_update_status(self._id, '-1')

        else:
            self.log.show('Nao foi Possivel confirmar o Login, tentando novamente')
            self.change_profile_name(name)   

    def change_profile_status(self, status, callback=False):
        status = urllib.parse.unquote(status)
        self.log.show('ACAO -> Alterar status do Perfil')
        if self.check_login():
            self.log.show('Iniciando tela do Whatsapp')
            self.log.show('Aguardando Confirmacao de Perfil')

            try:
                self.evo.wait_for_selector('#side > header > div._2umId > div', 30)
                self.driver.find_element_by_css_selector('#side > header > div._2umId > div').click()
                self.log.show('Clicando no Perfil')
                time.sleep(1)

                try:
                    self.evo.wait_for_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > header > div > div._1xGbt', 10)
                    self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(4) > div.ogWqZ._2-h1L > div._1DTd4._1G2k- > span.mOUqK > div > span').click()
                    
                    try:
                        self.evo.wait_for_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(4) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text', 10)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(4) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(Keys.CONTROL + 'a')
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(4) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(Keys.BACKSPACE)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(4) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(status)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div:nth-child(4) > div.ogWqZ._2-h1L._31WRs > div._1DTd4 > div > div._2S1VP.copyable-text.selectable-text').send_keys(Keys.ENTER)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > header > div > div.SFEHG > button > span').click()
                        time.sleep(1)
                        self.evo.api_update_status(self._id, '6')
                        self.log.show('Status alterado para ' + status + ' Com Sucesso')
                        
                        try:
                            if callback != False:
                                callback(self) 
                        except Exception as ex:
                            self.log.show('ERRO Callback ->' + ex)

                    except Exception as ex:
                        self.log.show(ex)
                        self.evo.api_update_status(self._id, '-1')
                    
                except Exception as ex:
                    self.log.show('Erro ao Carregar DIV do Perfil')
                    self.log.show(ex)
                    self.evo.api_update_status(self._id, '-1')
            except:
                self.log.show('Nao foi Possivel clicar no Perfil')
                self.evo.api_update_status(self._id, '-1')

        else:
            self.log.show('Nao foi Possivel confirmar o Login, tentando novamente')
            self.change_profile_status(status)

    def change_profile_image(self, image):
        self.log.show('ACAO -> Alterar imagem do Perfil')

        if self.check_login():
            self.log.show('Iniciando tela do Whatsapp')
            self.log.show('Aguardando Confirmacao de Perfil')
            self.log.show('ID ->' + self._id)
            self.log.show('ID ->' + self.api_path)

            try:
                self.evo.wait_for_selector('#side > header > div._2umId > div', 30)
                self.driver.find_element_by_css_selector('#side > header > div._2umId > div').click()
                self.log.show('Clicando no Perfil')
                try:
                    self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > div > div._12fSF > div > input[type="file"]').send_keys(image)
                    try:
                        self.evo.wait_for_selector('#app > div > span:nth-child(2) > div > div > div > div > div > div > span > div > div > div._18wuJ > span > div > div', 10)
                        
                        btn_confirm_img = self.driver.find_element_by_class_name('_3hV1n')
                        self.driver.execute_script("arguments[0].click();", btn_confirm_img)
                        time.sleep(2)
                        self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP.k1feT > span > div > div > header > div > div.SFEHG > button > span').click()
                        self.log.show('Imagem do perfil alterada com Sucesso')
                        self.evo.api_update_status(self._id, '6')
                        return True

                    except Exception as ex:
                        self.log.show('Erro ao Carregar Selecao de Imagens ->' + str(ex))
                        self.evo.api_update_status(self._id, '-1')
                    
                except Exception as ex:
                    self.log.show('Erro ao Carregar DIV do Perfil')
                    self.evo.api_update_status(self._id, '-1')
                    self.log.show(ex)

            except Exception as ex:
                self.log.show('Erro ->' + str(ex))

        else:
            self.log.show('Nao foi Possivel confirmar o Login, tentando novamente')
            self.change_profile_image(image)
    
    def validate_contact(self, _id):
        self.log.show('ACAO -> Validar Contato')
        self.log.show('Iniciando tela do Whatsapp')
        try:
            self.evo.wait_for_selector_visible('#app > div > span:nth-child(2) > div > span > div > div > div > div > div > div._3lLzD', 2)
            self.evo.api_update_status(self._id, '-1')
            self.driver.find_element_by_css_selector('#app > div > span:nth-child(2) > div > span > div > div > div > div > div > div._3QNwO > div').click()
            self.log.show('Contato Invalido')
            return False

        except:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text',2)
            self.log.show('Contato Valido')
            return True

    def send_text_message(self, msg):
        self.log.show('ACAO -> Enviar Mensagem de Texto')
        self.log.show('ID ->' + self._id)
        self.log.show('ID ->' + self.api_path)

        texto =  msg.replace('\n', '\\n')
        texto = urllib.parse.unquote_plus(texto)

        try:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text', 15)
            cmd_script = "var evt = new Event('input', {bubbles: true}); var input = document.querySelector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text'); input.innerHTML = `"+str(texto)+"`; input.dispatchEvent(evt); document.querySelector('#main > footer > div._3pkkz.copyable-area > div:nth-child(3) > button').click();"
            self.driver.execute_script(cmd_script)
            
            self.log.show('Mensagem Enviada com Sucesso')
            self.evo.api_update_status(self._id, '3')

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[send_message] -> '+str(ex))
            self.evo.api_update_status(self._id, '99')
            return False

        try:
            self.evo.wait_for_selector('._32uRw', 3)
            self.log.show('Confirmacao de Recebimento de Mensagem [OK]')
            self.evo.api_update_status(self._id, '4')

        except Exception as ex:
            pass

    def send_image_message(self, image, msg=" "):
        self.log.show('ACAO -> Enviar Mensagem com Imagem')
        texto =  msg.replace('\n', '\\n')
        texto = urllib.parse.unquote_plus(texto)

        try:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text', 15)
            time.sleep(1)
            self.driver.find_element_by_xpath('//*[@id="main"]/header/div[3]/div/div[2]/div').click()
            self.driver.find_element_by_xpath('//*[@id="main"]/header/div[3]/div/div[2]/span/div/div/ul/li[1]/button/input').send_keys(image)
            
            try:
               self.evo.wait_for_selector('#app > div > div > div.MZIyP > div._3q4NP._1Iexl > span > div > span > div > div > div._2sNbV._3ySAH > div > span > div > div._18KIe > div > div.CzI8E > div._3F6QL.bsmJe._1ZxJu > div._2S1VP.copyable-text.selectable-text', 10)
               cmd_script = "var evt = new Event('input', {bubbles: true}); var input = document.querySelector('#app > div > div > div.MZIyP > div._3q4NP._1Iexl > span > div > span > div > div > div._2sNbV._3ySAH > div > span > div > div._18KIe > div > div.CzI8E > div._3F6QL.bsmJe._1ZxJu > div._2S1VP.copyable-text.selectable-text'); input.innerHTML = `"+str(texto)+"`; input.dispatchEvent(evt); document.querySelector('#app > div > div > div.MZIyP > div._3q4NP._1Iexl > span > div > span > div > div > div._2sNbV._3ySAH > span:nth-child(3) > div > div').click();"
               self.driver.execute_script(cmd_script)
               self.evo.api_update_status(self._id, '3')

               time.sleep(1)

               try:
                   self.evo.wait_for_selector('._32uRw', 5)
                   txt_hora_msg = self.driver.find_element_by_css_selector('._3EFt_').text
                   self.log.show('Ultima Mensagem Entregue em -> ' + txt_hora_msg)
                   self.log.show('Confirmacao de Recebimento de Mensagem [OK]')
                   self.evo.api_update_status(self._id, '4')
                   time.sleep(3)
               
               except Exception as ex:
                   self.log.show(str(ex), 'ERRO[send_image] -> '+str(ex))
                   self.evo.api_update_status(self._id, '99')

            except Exception as ex:
                self.log.show(str(ex), 'ERRO[send_image] -> '+str(ex))
                self.evo.api_update_status(self._id, '99')

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[send_message] -> '+str(ex))
            self.evo.api_update_status(self._id, '99')
            return False
    
    def send_contact(self, name):
        self.log.show('ACAO -> Enviar Contato: ' + name)
        self.driver.find_element_by_xpath('//*[@id="main"]/header/div[3]/div/div[2]/div').click()
        time.sleep(1)
        self.driver.find_element_by_xpath('//*[@id="main"]/header/div[3]/div/div[2]/span/div/div/ul/li[4]/button').click()

        try:
            self.evo.wait_for_selector_visible('#app > div > span:nth-child(2) > div > span > div > div > div > div > div > div', 10)
            self.driver.find_element_by_css_selector('#app > div > span:nth-child(2) > div > span > div > div > div > div > div > div > div:nth-child(2) > div > label > input').send_keys(name)
            time.sleep(1)

            try:
                self.evo.wait_for_selector_visible('#app > div > span:nth-child(2) > div > span > div > div > div > div > div > div > div._1vDUw._2sNbV > div > div > div > div:nth-child(2) > div > div:nth-child(2) > div', 5)
                self.driver.find_element_by_css_selector('#app > div > span:nth-child(2) > div > span > div > div > div > div > div > div > div._1vDUw._2sNbV > div > div > div > div:nth-child(2) > div > div:nth-child(2) > div').click()

                try:
                    self.evo.wait_for_selector_visible('//*[@id="app"]/div/span[2]/div/span/div/div/div/div/div/div/span/div/div', 5, 'xpath')
                    self.driver.find_element_by_xpath('//*[@id="app"]/div/span[2]/div/span/div/div/div/div/div/div/span/div/div').click()
                    self.log.show('Contato enviado Com Sucesso')
                    
                    try:
                        self.evo.wait_for_selector_visible('//*[@id="app"]/div/span[2]/div/span/div/div/div/div/div/div/div[2]/div/div', 5, 'xpath')
                        self.driver.find_element_by_xpath('//*[@id="app"]/div/span[2]/div/span/div/div/div/div/div/div/div[2]/div/div').click()
                        self.log.show('Contato enviado Com Sucesso')
                        return True
                    
                    except Exception as ex:
                        self.log.show(str(ex), 'ERRO[2] -> '+str(ex))
                        self.evo.api_update_status(self._id, '99')
                        return False
                
                except Exception as ex:
                    self.log.show(str(ex), 'ERRO[2] -> '+str(ex))
                    self.evo.api_update_status(self._id, '99')
                    return False

            except Exception as ex:
                self.log.show(str(ex), 'ERRO[1] -> '+str(ex))
                self.evo.api_update_status(self._id, '99')
                return False
           

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[0] -> '+str(ex))
            self.evo.api_update_status(self._id, '99')
            return False

    def insert_contact(self, number):
        time.sleep(1)
        self.log.show('Injetando Contato -> ' + number)
        try:
            self.driver.execute_script("document.querySelector('#side > header').innerHTML = '<a id=\"id_contato\" href=\"https://web.whatsapp.com/send?phone="+str(number)+"\">"+str(number)+"</a>'")
            
            try:
                self.evo.wait_for_selector('#id_contato', 3)
                
                try:
                    self.evo.wait_for_selector('#id_contato', 3)
                    self.driver.find_element_by_id('id_contato').click()
                    self.log.show('Contato injetado com Sucesso')
                
                except Exception as ex:
                    self.log.show(str(ex), 'ERRO[insert_contact][2] -> '+str(ex))
                    return False 


            except Exception as ex:
                self.log.show(str(ex), 'ERRO[insert_contact][1] -> '+str(ex))
                return False    
        
        except Exception as ex:
            self.log.show(str(ex), 'ERRO[insert_contact][0] -> '+str(ex))
            return False
    
    def me_send_last_msg(self):
        self.log.show('ACAO -> Verificar ultima mensagem recebida')

        try:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text', 15)
            messages  = self.driver.find_elements_by_css_selector('.vW7d1')
            pos = len(messages)-1

            last_msg = messages[pos]

            try:
                last_msg.find_element_by_css_selector('.message-out')
                return False
            except:

                try:
                    last_msg.find_element_by_css_selector('.message-in')
                    return True
                except:
                    return False

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[send_message] -> '+str(ex))
            return False

    def get_last_message(self):
        self.log.show('ACAO -> Pegar ultima mensagem enviada')

        try:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text', 15)
            messages  = self.driver.find_elements_by_css_selector('.vW7d1')
            pos = len(messages)-1

            last_msg = messages[pos]

            try:
                last_msg.find_element_by_css_selector('.message-out')
                return False
            except:

                try:
                    last_msg.find_element_by_css_selector('.message-in')
                    last_msg = last_msg.find_element_by_css_selector('.copyable-text').text
                    self.log.show('Msg -> ' + last_msg)
                    return last_msg
                except:
                    return False

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[send_message] -> '+str(ex))
            return False

    def listening_chats(self):
        self.log.show('ACAO -> Aguardando Novas entradas no chat')

        try:
            elems = self.driver.find_elements_by_class_name('CxUIE')
            
            if len(elems) > 0:
                self.log.show('Novo chat encontrado')
                pos = len(elems)-1
                last_chat = elems[pos]
                last_chat.click()
                return True
                
            
            else:
                self.log.show('Nenhum chat pendente')
                return False

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[listening_chats] -> '+str(ex))
            return False

    def is_active_chat(self):
        self.log.show('ACAO -> Verificando Chat Ativo')

        try:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text', 1)
            self.log.show('Chat Ativo')
            return True
    

        except:
            try:
                self.evo.wait_for_selector('#app > div > div > div._3q4NP._1Iexl > div > div', 1)
                self.log.show('Nenhum chat Ativo')
                return False

            except Exception as ex:
                self.log.show(str(ex), 'ERRO[is_active_chat] -> '+str(ex))
                return False

    def open_chat_info(self):
        self.log.show('ACAO -> Abrindo informações do Chat')

        try:
            self.evo.wait_for_selector('#main > footer > div._3pkkz.copyable-area > div._1Plpp > div > div._2S1VP.copyable-text.selectable-text', 1)
            self.driver.find_element_by_css_selector('#main > header > div._5SiUq').click()
            self.log.show('Dialogo de Informacoes Carregados')
            return True
    

        except Exception as ex:
            self.log.show(str(ex), 'ERRO[open_chat_info] -> '+str(ex))
            return False
          
    def get_chat_number(self):
        self.log.show('ACAO -> Numero do Chat')
        if self.is_active_chat():
            try:
                self.evo.wait_for_selector('#app > div > div > div.MZIyP > div._3q4NP._2yeJ5 > span > div > span > div > div > div > div:nth-child(4) > div:nth-child(3)',3)
                numero = self.driver.find_element_by_css_selector('#app > div > div > div.MZIyP > div._3q4NP._2yeJ5 > span > div > span > div > div > div > div:nth-child(4) > div:nth-child(3)').text
                numero = self.evo.str_clean(numero)
                self.log.show('Numero -> ' + numero)
                return numero
            except:
                self.log.show('Nao foi possivel obter o numero do Chat')
                return False
        else:
            self.log.show('Nenhum chat ativo')
            return False

    def search_term(self,term):
        self.log.show('ACAO -> Buscar Termos')
        if term == '1':
            return 'Bem vindo a Empresa X Somos tal pessoa lorem ipson'
        if term == '2':
            return 'Opçao Escolhida: 2 -> Lorem Ipsun'

        if term == '3':
            return 'Opçao Escolhida: 3 -> Lorem Ipsun'
        
        else:
            return 'Você não selecionou uma opção válida :/'

    def get_conversation(self):
        file = "conversas.txt"
        lines = open(file, mode='r', encoding='utf-8').read().splitlines()
        line = random.choice(lines)
        self.log.show('CONVERSATION ->' + str(line))
        return str(line)

    def get_handshake(self):
        file = "interacoes.txt"
        lines = open(file, mode='r', encoding='utf-8').read().splitlines()
        line = random.choice(lines)
        self.log.show('HANDSHAKE ->' + str(line))
        return str(line)
    
    def get_thank_msg(self):
        file = "agradecimento.txt"
        lines = open(file, mode='r', encoding='utf-8').read().splitlines()
        line = random.choice(lines)
        self.log.show('HANDSHAKE ->' + str(line))
        return str(line)
