from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.by import By
import re
import requests
import log

class evo():
    def __init__(self, driver):
        self.driver = driver
        self.api_path = "http://crm2.marketingmidia9.com.br/whatsapp/"
        self.api_key = False
        self.log = log.Log()
    
    def wait_for_selector(self, selector, timer, by = 'selector'):
        if by == 'selector':
            WebDriverWait(self.driver, timer).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
        if by == 'class':
            WebDriverWait(self.driver, timer).until(EC.presence_of_element_located((By.CLASS_NAME, selector)))
        if by == 'xpath':
            WebDriverWait(self.driver, timer).until(EC.presence_of_element_located((By.XPATH, selector)))
            
    def wait_for_selector_visible(self, selector, timer, by = 'selector'):
        if by == 'selector':
            WebDriverWait(self.driver, timer).until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
        if by == 'class':
            WebDriverWait(self.driver, timer).until(EC.visibility_of_element_located((By.CLASS_NAME, selector)))
        if by == 'xpath':
            WebDriverWait(self.driver, timer).until(EC.visibility_of_element_located((By.XPATH, selector)))
    
    def get_text_by_selector(self, selector):
        return self.driver.find_element_by_css_selector(selector).text

    def str_clean(self, string):
        string = ''.join(e for e in string if e.isalnum())
        return string
    
    def api_whatsapp_reply(self, numero, msg):
        data = {'numero':numero, 'msg':msg}
        requests.post(self.api_path + 'check_queue_replys/' + str(self.api_key), data)
        self.log.show('Salvando Resposta -> ' + str(msg))

    def api_update_status(self, _id, status):
        self.log.show('Atualizando status da Mensagem no Banco para -> ' + str(status) + '| id ->' + str(_id))
        requests.get(self.api_path + '/update_status/' +str( _id) + '/' + str(status))
